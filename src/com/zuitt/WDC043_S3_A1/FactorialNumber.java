package com.zuitt.WDC043_S3_A1;

import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args){

        Scanner in = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed");
//        int num = in.nextInt();
//
//        int count = 1;
//        int answer = 1;
//        while(count<=num){
//
//            answer*= count;
//            count++;
//        }
//
//        System.out.println("The factorial of "+ num + " is "+ answer);

        int answer = 1;
        try{
            int num = in.nextInt();
            if(num == 0) {
                System.out.println("The factorial of " + num + " is "+ 1);
            }else if(num >= 1) {
                for (int i = 1; i <= num; i++) {
                    answer *= i;
                }
                System.out.println("The factorial of " + num + " is " + answer);
            }else{
                System.out.println("Invalid input. Please enter an integer and a number greater than 0");
            }
        }catch(Exception e){
            System.out.println("Invalid input. Please enter an integer and a number greater than 0");
            e.printStackTrace();
        }
    }
}
